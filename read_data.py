#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tensorflow as tf

import config as cfg

import numpy as np
import pandas as pd
import os
import time
import scipy.misc as misc
import matplotlib.pyplot as plt


def read_input_csv(data_dir):
    f=open(data_dir)
    df=pd.read_csv(f,header=None)
    data = df.iloc[:,:].values # 获取所有数据，输出为numpy形式
    f.close()
    return data

##a = read_input_csv(cfg.data_dir+'/Train_A/Train_A/Train_A_001.csv')
##print(a[0])
##print(a.shape)


def read_output_csv(data_dir):
    f=open(data_dir)
    df=pd.read_csv(f)
    data = df.iloc[:,:].values # 获取所有数据，输出为numpy形式
    data = data[:,1:]
    f.close()
    return data

##a = read_output_csv(cfg.data_dir+'/Train_A/Train_A_wear.csv')
##print(a[0])
##print(a.shape)


''' 把输入的平均值保存 '''
def save_input_mean():
    Train_A_data = []
    Train_B_data = []
    Test_data = []
    for i in range(315):
        print(cfg.data_dir+'/Train_A/Train_A/Train_A_%03d.csv'%(i+1))
        temp = read_input_csv(cfg.data_dir+'/Train_A/Train_A/Train_A_%03d.csv'%(i+1))
        temp = np.fabs(temp)
        temp = np.mean(temp,axis=0)
        Train_A_data.append(temp)

        print(cfg.data_dir+'/Train_B/Train_B/Train_B_%03d.csv'%(i+1))
        temp = read_input_csv(cfg.data_dir+'/Train_B/Train_B/Train_B_%03d.csv'%(i+1))
        temp = np.fabs(temp)
        temp = np.mean(temp,axis=0)
        Train_B_data.append(temp)

        print(cfg.data_dir+'/Test/Test/Test_%03d.csv'%(i+1))
        temp = read_input_csv(cfg.data_dir+'/Test/Test/Test_%03d.csv'%(i+1))
        temp = np.fabs(temp)
        temp = np.mean(temp,axis=0)
        Test_data.append(temp)

    Train_A_data = pd.DataFrame(np.array(Train_A_data)) # header:原第一行的索引，index:原第一列的索引
    Train_A_data.to_csv(cfg.data_dir+'/Train_A/Train_A_input.csv')
    Train_B_data = pd.DataFrame(np.array(Train_B_data))
    Train_B_data.to_csv(cfg.data_dir+'/Train_B/Train_B_input.csv')
    Test_data = pd.DataFrame(np.array(Test_data))
    Test_data.to_csv(cfg.data_dir+'/Test/Test_input.csv')

#save_input_mean()

def read_input_mean(data_dir):
    f=open(data_dir)
    df=pd.read_csv(f)
    data = df.iloc[:,:].values # 获取所有数据，输出为numpy形式
    data = data[:,1:]
    f.close()
    return data

##a = read_input_mean(cfg.data_dir+'/Train_A/Train_A_input.csv')
##print(a[0])


inputs = read_input_mean(cfg.data_dir+'/Train_A/Train_A_input.csv')
mean_value = np.mean(inputs,axis=0)
std_value = np.std(inputs,axis=0, dtype=float)

'''  初始化训练，验证，测试数据 '''
def producer_train_data():
    inputs = read_input_mean(cfg.data_dir+'/Train_A/Train_A_input.csv')
    temp = np.zeros([1,7])
    inputs = np.concatenate((temp,inputs),axis=0)
    inputs_norm = (inputs-np.mean(inputs,axis=0))/np.std(inputs,axis=0, dtype=float)
    expects = read_output_csv(cfg.data_dir+'/Train_A/Train_A_wear.csv')
    temp2 = np.zeros([1,3])
    expects = np.concatenate((temp2,expects),axis=0)
    expects_norm = expects/250.0
    return inputs_norm,expects_norm

def producer_val_data():
    inputs = read_input_mean(cfg.data_dir+'/Train_B/Train_B_input.csv')
    temp = np.zeros([1,7])
    inputs = np.concatenate((temp,inputs),axis=0)
    inputs_norm = (inputs-mean_value)/std_value
    expects = read_output_csv(cfg.data_dir+'/Train_B/Train_B_wear.csv')
    temp2 = np.zeros([1,3])
    expects = np.concatenate((temp2,expects),axis=0)
    expects_norm = expects/250.0
    return inputs_norm,expects_norm

def producer_test_data():
    inputs = read_input_mean(cfg.data_dir+'/Test/Test_input.csv')
    temp = np.zeros([1,7])
    inputs = np.concatenate((temp,inputs),axis=0)
    inputs_norm = (inputs-np.mean(inputs,axis=0))/np.std(inputs,axis=0, dtype=float)
    return inputs_norm




train_inputs, train_expects = producer_train_data()
val_inputs, val_expects = producer_val_data()    
test_inputs = producer_test_data()

##temp = (np.zeros([1,7])-mean_value)/std_value
##train_inputs = np.concatenate((temp,train_inputs),axis=0)
##val_inputs = np.concatenate((temp,val_inputs),axis=0)
##test_inputs = np.concatenate((temp,test_inputs),axis=0)
##temp2 = np.zeros([1,3])
##train_expects = np.concatenate((temp2,train_expects),axis=0)
##val_expects = np.concatenate((temp2,val_expects),axis=0)

##print(train_inputs[0])
##print(train_expects[0])
##print(val_inputs[0])
##print(val_expects[0])
##print(test_inputs[0])

# 显示读取的数据
#train_times = np.array(range(0,316)).reshape([-1,1])
#plt.plot(train_times,train_inputs)
#plt.plot(train_times,train_expects[:,0])
#plt.plot(train_times,val_inputs)
#plt.plot(train_times,val_expects[:,0:3])
#plt.plot(train_times,test_inputs)
#plt.show()


train_input_con = np.concatenate((train_inputs[1:316,:],train_expects[0:315,:]),axis=1)
train_expects = train_expects[1:316,:]
import random
def read_train_batch():
    inputs_batch = []
    expect_batch = []
    for i in range(cfg.train_batch_size):
        start_pos = random.randint(0,314)
        #start_pos = 1
        #print(start_pos)
        inputs_batch.append(train_input_con[start_pos,:])
        expect_batch.append(train_expects[start_pos,:])
    return inputs_batch,expect_batch

##batch = read_train_batch()
##print(batch[0][0])
##print(batch[1][0])


val_input_con = np.concatenate((val_inputs[1:316,:],val_expects[0:315,:]),axis=1)
val_expects = val_expects[1:316,:]
def read_val_batch():
    inputs_batch = []
    expect_batch = []
    for i in range(cfg.val_batch_size):
        start_pos = random.randint(0,314)
        #print(start_pos)
        inputs_batch.append(val_input_con[start_pos,:])
        expect_batch.append(val_expects[start_pos,:])
    return inputs_batch,expect_batch


def read_test_batch(index):
    inputs_batch = []
    for i in range(1):
        start_pos = index #random.randint(0,314)
        #print(start_pos)
        inputs_batch.append(val_inputs[start_pos:start_pos+cfg.time_step,:])
    return inputs_batch


