#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import os




###设置常量
time_step = 1      #时间步
rnn_unit = 10       #hidden layer units
input_size = 10      #输入层维度
output_size = 3      #输出层维度

NUM_LAYERS = 1


# 变量初始化 没有设置环境变量就按默认值返回数据
data_dir = str(os.getenv("TF_DATA_DIR", 'D:/workspace/python/tensorflow/datasets/cuttor_wear'))
train_batch_size = int(os.getenv("TF_TRAIN_BATCH_SIZE", 20))
val_batch_size = int(os.getenv("TF_VAL_BATCH_SIZE", 20))
train_steps = int(os.getenv("TF_TRAIN_STEPS", 6000))
learning_rate = float(os.getenv("TF_LR", 0.001))


# 模型保存目录相关变量
model_dir = str(os.getenv("TF_MODEL_DIR", 'Model'))
model_recent_dir = str(os.getenv("TF_MODEL_RECENT_DIR", 'Model'))


# checkpoint,logs目录建立
if not os.path.exists(model_dir+'/logs'):
    os.makedirs(model_dir+'/logs')
if not os.path.exists(model_dir+'/checkpoint'):
    os.makedirs(model_dir+'/checkpoint')
summary_dir = model_dir+'/logs/'
ckpt_dir = model_dir+'/checkpoint/'
# 上一次保存的checkpoint路径
ckpt_recent_dir = model_recent_dir+'/checkpoint/'



