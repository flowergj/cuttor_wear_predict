#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import tensorflow.contrib.slim as slim

import numpy as np
import os
import time
import scipy.misc as misc

import config as cfg
import read_data

graph_train = tf.Graph()
with graph_train.as_default():
    sess = tf.Session()

    X = tf.placeholder(tf.float32, [None,cfg.input_size], name='inputs') 
    Y = tf.placeholder(tf.float32, [None,cfg.output_size], name='expects')

    keep_dropout = tf.placeholder("float")
    
    output = tf.contrib.layers.fully_connected(X,10,activation_fn=tf.nn.relu)
    output = slim.dropout(output, keep_dropout, scope='dropout1')
    predictions = tf.contrib.layers.fully_connected(output, cfg.output_size, activation_fn=tf.nn.sigmoid)
    labels = Y

    '''定义损失函数，这里为正常的均方误差'''
    loss = tf.losses.mean_squared_error(predictions, labels)
    #loss = -tf.reduce_mean(labels*tf.log(predictions))

    '''定义优化器各参数'''
    train_op = tf.train.AdamOptimizer(cfg.learning_rate).minimize(loss)
##    train_op = tf.contrib.layers.optimize_loss(loss,
##                                               tf.train.get_global_step(),
##                                               optimizer='Adagrad',
##                                               learning_rate=cfg.learning_rate)

    ''' summary '''
    tf.summary.scalar('total_loss', loss)
    tf.summary.scalar('learning_rate', cfg.learning_rate)
    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter(cfg.summary_dir, sess.graph)
    
    init = tf.global_variables_initializer()
    sess.run(init)

    ''' 恢复数据 '''
    saver = tf.train.Saver()
    if os.path.exists(cfg.ckpt_recent_dir+'checkpoint'):
        print('恢复模型...')
        saver.restore(sess, cfg.ckpt_recent_dir+'model.ckpt')
        print('恢复模型完成')


def train():
    for _step in range(cfg.train_steps):
        input_batch,expect_batch = read_data.read_train_batch()
        feed_dict = {X:input_batch,Y:expect_batch,keep_dropout:0.5}
        _,_loss,_pred = sess.run([train_op,loss,predictions], feed_dict=feed_dict)
        
        if _step%100==0:
            print(_step, _loss)

        # 保存模型
        if _step%1000 == 0 and _step!=0:
            saver.save(sess, cfg.ckpt_dir+'model.ckpt')



import matplotlib.pyplot as plt
def val():
    for _step in range(cfg.train_steps):
        input_batch,expect_batch = read_data.read_val_batch()
        feed_dict = {X:input_batch,Y:expect_batch,keep_dropout:1.0}
        _loss,_pred = sess.run([loss,predictions], feed_dict=feed_dict)
        
        print(_step, _loss)
        
        plot_result(cfg.val_batch_size, np.array(expect_batch).reshape([-1,3])[:,0], np.array(_pred).reshape([-1,3])[:,0]);



def test(input_data, expect):
    all_pred = []
    current_pred = np.array([[0,0,0]])
    test_inputs = input_data
    for i in range(1,316): #-cfg.time_step
        test_input_con = np.concatenate((test_inputs[i,:].reshape([-1,7]),current_pred.reshape([-1,3])),axis=1)
        #print(test_input_con)
        input_batch = test_input_con.reshape([-1,10])
        feed_dict = {X:input_batch,keep_dropout:1.0}
        _pred = sess.run(predictions, feed_dict=feed_dict)
        current_pred = _pred
        all_pred.append(_pred[0]) #*250.0
        
        print(i, _pred[0][0]*250,_pred[0][1]*250, _pred[0][2]*250)
        
    plot_result(315, expect[:,0:3], np.array(all_pred).reshape([-1,3])[:,0:3]);


def plot_result(size, expects, all_pred):
    times = np.arange(size) #-cfg.time_step
    plt.plot(times.reshape([-1,1]), expects)
    plt.plot(times.reshape([-1,1]), all_pred)
    plt.show()
    

def plot_Train_Val_Test():
    test(read_data.train_inputs, read_data.train_expects)
    test(read_data.val_inputs, read_data.val_expects)
    test(read_data.test_inputs, read_data.train_expects)
    


if __name__=='__main__':
##    train()
##    val()
##    test(read_data.test_inputs, read_data.train_expects)

    plot_Train_Val_Test();


